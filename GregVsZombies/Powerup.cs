﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GregVsBroccoli
{
    class Powerup
    {
        public Texture2D PowerupTexture;

        public Vector2 Position;

        public Rectangle BoundingBox;

        public bool Broccoli;
        public bool Shooting;
        public bool Movement;
        public bool Health;

        public int Width
        {
            get { return PowerupTexture.Width; }
        }

        public int Height
        {
            get { return PowerupTexture.Height; }
        }


        public void Initialize(Texture2D texture, Vector2 pos, bool broc, bool shoot, bool move, bool health)
        {
            PowerupTexture = texture;
            Broccoli = broc;
            Shooting = shoot;
            Movement = move;
            Health = health;
            Position = pos;

            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, texture.Width, texture.Height);
        }

        public void Update(GraphicsDevice device)
        {
            Position.Y += (Vector2.UnitY * 2).Y;
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, PowerupTexture.Width, PowerupTexture.Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(PowerupTexture, Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
