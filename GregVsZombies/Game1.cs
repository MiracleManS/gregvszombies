﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace GregVsBroccoli
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D broccoli;
        Texture2D health;
        Texture2D gregTexture;
        Player player;
        Vector2 playerPosition;
        KeyboardState oldState;
        SpriteFont gameFont;

        SoundEffect explosionSound;
        SoundEffect starShotSound;
        SoundEffect powerUpSound;
        SoundEffect chewingSound;
        Song backgroundMusic;
        Song gameOver;

        KeyboardState simulateOldState;

        KeyboardState gameStartOldState;

        TimeSpan upHealth;

        int brocCount;

        TimeSpan powerupTime;
        TimeSpan prevPowerupTime;

        List<Broccoli> brocList;

        List<Explosion> expList;

        List<Powerup> powerupList;

        //Projectiles!
        List<Projectile> bullets;
        TimeSpan prevBulletSpawnTime;
        TimeSpan bulletSpawnTime;

        List<Greg> enemies;
        TimeSpan prevGregSpawnTime;
        TimeSpan gregSpawnTime;

        Vector2 gregStart;
        int gregCount = 1;

        int Score = 0;

        bool paused = false;
        bool gameStart = true;
        bool endMusic = false;
        bool bgPlaying = true;

        TimeSpan pauseTime = TimeSpan.Zero;

        public Game1()
        {
            this.Window.Title = "Greg vs Broccoli: A Minor Inconvenience";
            graphics = new GraphicsDeviceManager(this);

            

            //graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            player = new GregVsBroccoli.Player();

            gameFont = Content.Load<SpriteFont>("GameFont");
            gregTexture = Content.Load<Texture2D>("Face");

            bullets = new List<Projectile>();
            enemies = new List<Greg>();
            prevBulletSpawnTime = TimeSpan.Zero;
            bulletSpawnTime = TimeSpan.FromSeconds(0.2f);

            brocCount = 5;

            prevGregSpawnTime = TimeSpan.Zero;
            gregSpawnTime = TimeSpan.FromSeconds(1f);

            Random r = new Random();
            powerupTime = new TimeSpan(0, 0, r.Next(15, 30));
            prevPowerupTime = TimeSpan.Zero;

            var division = GraphicsDevice.Viewport.Width / (brocCount + 1);
            brocList = new List<Broccoli>();
            expList = new List<Explosion>();
            powerupList = new List<Powerup>();

            for (int i = 1; i <= brocCount; i++)
            {
                var broc = new Broccoli();
                broc.Initialize(Content.Load<Texture2D>("broccoli"), Content.Load<Texture2D>("broccoli2"), Content.Load<Texture2D>("broccoli3"), new Vector2(division * i, GraphicsDevice.Viewport.Height - 50));
                brocList.Add(broc);
            }
            gregStart = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.Width / 2, GraphicsDevice.Viewport.TitleSafeArea.Top);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            broccoli = this.Content.Load<Texture2D>("broccoli");

            explosionSound = this.Content.Load<SoundEffect>("explosion");
            starShotSound = this.Content.Load<SoundEffect>("star_shot");
            powerUpSound = this.Content.Load<SoundEffect>("powerup");
            chewingSound = this.Content.Load<SoundEffect>("chew");

            playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.Width / 2, GraphicsDevice.Viewport.TitleSafeArea.Bottom - broccoli.Height - broccoli.Height);

            health = this.Content.Load<Texture2D>("health");
            player.Initialize(Content.Load<Texture2D>("Player_anna"), playerPosition);

            backgroundMusic = this.Content.Load<Song>("background");
            MediaPlayer.Play(backgroundMusic);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.2f;

            gameOver = this.Content.Load<Song>("no_hope");


            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var newState = Keyboard.GetState();
            
            if(gameStart)
            {
                var gameStartNewState = Keyboard.GetState();
                
                gameStartOldState = gameStartNewState;
                UpdateGameStartScreen();
            }
            if(!player.Active || brocList.Count <= 0)
            {
                if (!endMusic && bgPlaying)
                {
                    MediaPlayer.Play(gameOver);
                    endMusic = true;
                    bgPlaying = false;
                }
            }

            if(player.Active && brocList.Count > 0)
            {
                if(endMusic && !bgPlaying)
                {
                    MediaPlayer.Play(backgroundMusic);
                    endMusic = false;
                    bgPlaying = true;
                }
            }

            if (!paused && !gameStart)
            {
                GameTime goodTime = new GameTime(gameTime.TotalGameTime, gameTime.ElapsedGameTime - pauseTime);
                Simulate(goodTime);
            }
            else
            {
                Stopwatch time = new Stopwatch();
                time.Start();
                if (newState.IsKeyDown(Keys.Delete) && !oldState.IsKeyDown(Keys.Delete))
                {
                    paused = false;
                    time.Stop();
                    pauseTime = time.Elapsed;
                }
            }

            oldState = newState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkGray);

            spriteBatch.Begin();
            if (gameStart)
            {                
                DrawStartScreen();
            }
            else
            {
                DrawScoreUpdate();

                foreach (var b in brocList)
                {
                    b.Draw(spriteBatch);
                }

                foreach (var e in expList)
                {
                    e.Draw(spriteBatch);
                }

                var numHearts = player.Health / 10;

                for (int h = 1; h <= numHearts; h++)
                {
                    spriteBatch.Draw(health, new Rectangle(GraphicsDevice.Viewport.Bounds.Right - (h * health.Width + 10), GraphicsDevice.Viewport.Bounds.Top, health.Width, health.Height), Color.DarkGray);
                }

                player.Draw(spriteBatch);

                for (int i = 0; i < bullets.Count; i++)
                    bullets[i].Draw(spriteBatch);

                for (int i = 0; i < enemies.Count; i++)
                    enemies[i].Draw(spriteBatch, gameTime);

                for (int i = 0; i < powerupList.Count; i++)
                    powerupList[i].Draw(spriteBatch);

                if (!player.Active || !brocList.Any())
                {

                    
                    DrawGameOverScreen();
                }


            }
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        protected void ShootBullet(Vector2 position, int offsetX, int offsetY)
        {
            position.X += offsetX;
            position.Y += offsetY;

            Projectile bullet = new Projectile();
            bullet.Initialize(Content.Load<Texture2D>("star"), position);

            bullets.Add(bullet);
        }

        protected void AddGreg(Vector2 position, int offsetX, int offsetY)
        {
            position.X += offsetX;
            position.Y += offsetY;

            Greg greg = new Greg();
            Vector2 velo = new Vector2();
            velo = Vector2.UnitY * ((gregCount / 25) + 1);
            int totHealth = (gregCount / 100 + 1);
            greg.Initialize(Content.Load<Texture2D>("Face"), Content.Load<Texture2D>("Face_open"), position, velo, GraphicsDevice, totHealth);
            enemies.Add(greg);
            gregCount++;
        }

        protected void AddPowerup(Vector2 position)
        {
            Powerup p = new Powerup();
            Random r = new Random();
            var startPos = gregStart;
            startPos.X = r.Next(GraphicsDevice.Viewport.TitleSafeArea.Left + 90, GraphicsDevice.Viewport.TitleSafeArea.Right - 90);
            p.Initialize(Content.Load<Texture2D>("upgrade"), startPos, false, true, false, true);
            powerupList.Add(p);

        }

        private void UpdateGameOverScreen()
        {
            enemies.Clear();
            bullets.Clear();
            expList.Clear();
            brocList.Clear();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter))
            {

                player.Active = true;
                player.Position = playerPosition;
                player.Health = 20;
                
                gregCount = 1;
                upHealth = TimeSpan.Zero;
                Score = 0;

                var division = GraphicsDevice.Viewport.Width / (brocCount + 1);
                brocList = new List<Broccoli>();
                for (int i = 1; i <= brocCount; i++)
                {
                    var broc = new Broccoli();
                    broc.Initialize(Content.Load<Texture2D>("broccoli"), Content.Load<Texture2D>("broccoli2"), Content.Load<Texture2D>("broccoli3"), new Vector2(division * i, GraphicsDevice.Viewport.Height - 50));
                    brocList.Add(broc);
                }
                //MediaPlayer.Play(backgroundMusic);

            }

        }

        private void UpdateGameStartScreen()
        {

            if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                player.Active = true;
                player.Position = playerPosition;
                player.Health = 20;
                enemies.Clear();
                bullets.Clear();
                expList.Clear();
                brocList.Clear();
                gregCount = 1;
                upHealth = TimeSpan.Zero;
                Score = 0;
                gameStart = false;

                var division = GraphicsDevice.Viewport.Width / (brocCount + 1);
                brocList = new List<Broccoli>();
                for (int i = 1; i <= brocCount; i++)
                {
                    var broc = new Broccoli();
                    broc.Initialize(Content.Load<Texture2D>("broccoli"), Content.Load<Texture2D>("broccoli2"), Content.Load<Texture2D>("broccoli3"), new Vector2(division * i, GraphicsDevice.Viewport.Height - 50));
                    brocList.Add(broc);
                }
            }

        }

        protected void UpdateCollision()
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                if (player.BoundingBox.Intersects(enemies[i].BoundingBox))
                {
                    player.Health -= 10;

                    enemies.RemoveAt(i);

                    // switch to an explosion animation
                    //AddExplosion(enemies[i].Position, -50, -30, enemies[i].xSpeed, 30);

                    if (player.Health <= 0)
                    {
                        //AddExplosion(player.Position, 0, -30, 0f, 200);
                        player.Active = false;
                        player.Position = Vector2.Zero;
                        
                    }
                }

                for (int j = 0; j < bullets.Count; j++)
                {
                    if (enemies.Count - 1 >= i && enemies[i] != null)
                    {
                        if (bullets[j].BoundingBox.Intersects(enemies[i].BoundingBox))
                        {

                            enemies[i].Health -= 1;
                            if (enemies[i].Health <= 0)
                            {
                                Explosion exp = new Explosion();
                                exp.Initialize(Content.Load<Texture2D>("Expl1"), Content.Load<Texture2D>("Expl2"), Content.Load<Texture2D>("Expl3"), enemies[i].Position);
                                expList.Add(exp);
                                explosionSound.Play(0.25f, 0.0f, 0.0f);
                                enemies.RemoveAt(i);
                            }
                            //AddExplosion(enemies[i].Position, -50, -30, -enemies[i].xSpeed, 30);
                            bullets.RemoveAt(j);
                            Score += 10;
                        }
                    }
                }
                if (enemies.Count - 1 >= i && enemies[i].Position.Y > GraphicsDevice.Viewport.TitleSafeArea.Bottom)
                {
                    if (brocList.Any())
                    {
                        enemies.RemoveAt(i);
                        chewingSound.Play(1f, 0.0f, 0.0f);
                        brocList[0].Health -= 1;
                        if (brocList[0].Health <= 0)
                        {
                            brocList.RemoveAt(0);
                        }
                    }
                }
            }

            for (int j = 0; j < powerupList.Count; j++)
            {
                for (int k = 0; k < bullets.Count; k++)
                {
                    if (powerupList.Count - 1 >= j && powerupList[j] != null)
                    {
                        if (bullets[k].BoundingBox.Intersects(powerupList[j].BoundingBox))
                        {

                            Explosion exp = new Explosion();
                            exp.Initialize(Content.Load<Texture2D>("Expl1"), Content.Load<Texture2D>("Expl2"), Content.Load<Texture2D>("Expl3"), powerupList[j].Position);
                            expList.Add(exp);
                            powerUpSound.Play(0.25f, 0.0f, 0.0f);
                            Powerup claimed = powerupList[j];
                            if (claimed.Movement)
                            {
                                player.Velocity = (int)(player.Velocity * 1.25);
                            }
                            if (claimed.Health)
                            {
                                player.Health += 10;
                                if (player.Health >= 50)
                                    player.Health = 50;
                            }
                            if (claimed.Shooting)
                            {
                                var ticks = bulletSpawnTime.Ticks;
                                long count = (long)(ticks/1.25);
                                bulletSpawnTime = TimeSpan.FromTicks(count);
                            }
                            if (claimed.Broccoli)
                            {
                                var b = brocList.FirstOrDefault();
                                if (b.Health < 3)
                                    b.Health = 3;
                                else
                                {
                                    if (brocList.Count < 5)
                                    {
                                        Broccoli bb = new Broccoli();
                                        var division = GraphicsDevice.Viewport.Width / (brocList.Count + 1);
                                        bb.Initialize(Content.Load<Texture2D>("broccoli"), Content.Load<Texture2D>("broccoli2"), Content.Load<Texture2D>("broccoli3"), new Vector2(division, GraphicsDevice.Viewport.Height - 50));
                                        brocList.Add(bb);
                                    }
                                }
                            }
                            powerupList.RemoveAt(j);
                            //AddExplosion(enemies[i].Position, -50, -30, -enemies[i].xSpeed, 30);
                            bullets.RemoveAt(k);
                        }
                    }
                }
            }
        }

        private void Simulate(GameTime gameTime)
        {
            // Lots and lots of code!
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            var newState = Keyboard.GetState();
            if(gameStart)
            {
                UpdateGameStartScreen();
            }

            if (player.Active && brocList.Any())
            {

                

                if (newState.IsKeyDown(Keys.Delete) && !simulateOldState.IsKeyDown(Keys.Delete))
                {
                    paused = true;
                }



                // TODO: Add your update logic here

                player.Update(GraphicsDevice, 5 * ((gregCount / 25) + 1));
                if (gregCount % 25 == 0 && (upHealth == null || upHealth < gameTime.ElapsedGameTime))
                {

                    player.Health += 10;
                    if (player.Health >= 50)
                        player.Health = 50;
                    upHealth = gameTime.ElapsedGameTime;
                }

                if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    if (gameTime.TotalGameTime - prevBulletSpawnTime > bulletSpawnTime)
                    {
                        prevBulletSpawnTime = gameTime.TotalGameTime;
                        ShootBullet(player.Position, 0, 10);
                        //starShotSound.Play();
                    }

                }

                if (gameTime.TotalGameTime - prevGregSpawnTime > gregSpawnTime)
                {
                    Random r = new Random();
                    gregStart.X = r.Next(GraphicsDevice.Viewport.TitleSafeArea.Left + 90, GraphicsDevice.Viewport.TitleSafeArea.Right - 90);
                    prevGregSpawnTime = gameTime.TotalGameTime;
                    AddGreg(gregStart, 0, 0);
                }

                if (gameTime.TotalGameTime - prevPowerupTime > powerupTime)
                {
                    Random r = new Random();
                    Vector2 v = new Vector2(r.Next(GraphicsDevice.Viewport.TitleSafeArea.Left, GraphicsDevice.Viewport.TitleSafeArea.Right), 0);
                    prevPowerupTime = gameTime.TotalGameTime;
                    AddPowerup(v);

                }

                UpdateCollision();

                for (int i = 0; i < bullets.Count; i++)
                    bullets[i].Update(gameTime);

                foreach (var p in powerupList)
                {
                    p.Update(GraphicsDevice);
                }


                foreach (var b in brocList)
                {
                    b.Update(GraphicsDevice);
                }

                for (int e = 0; e < expList.Count; e++)
                {
                    expList[e].Health--;
                    expList[e].Update(GraphicsDevice);
                    if (expList[e].Health == 0)
                        expList.RemoveAt(e);

                }

                for (int i = 0; i < enemies.Count; i++)
                    enemies[i].Update(gameTime);
            }
            else
            {
                UpdateGameOverScreen();
            }
            simulateOldState = newState;
        }

        private void DrawStartScreen()
        {
            var powerup = Content.Load<Texture2D>("upgrade");
            var center = GraphicsDevice.Viewport.TitleSafeArea.Center;
            center.Y -= 100;
            var left = GraphicsDevice.Viewport.TitleSafeArea.Left;


            spriteBatch.DrawString(gameFont, "Arrow keys move. Hold spacebar to fire. \nStop the Gregs from getting to the bottom of the screen!", new Vector2(left + powerup.Width, center.Y - (powerup.Height / 2)), Color.Black);

            spriteBatch.Draw(powerup, new Vector2(left, center.Y), Color.DarkGray);
            spriteBatch.DrawString(gameFont, " Increases fire rate. Adds health.", new Vector2(left + powerup.Width, center.Y + (powerup.Height / 2)), Color.Black);

            spriteBatch.Draw(gregTexture, new Vector2(left, center.Y + powerup.Height + 20), Color.DarkGray);
            spriteBatch.DrawString(gameFont, " Shoot the Gregs to protect your precious broccoli!!!!", new Vector2(left + gregTexture.Width, center.Y + powerup.Height + (gregTexture.Height / 2)), Color.Black);
            spriteBatch.DrawString(gameFont, "Press enter to start playing!", new Vector2(GraphicsDevice.Viewport.TitleSafeArea.Center.X - 50, GraphicsDevice.Viewport.TitleSafeArea.Top + 50), Color.Black);
        }


        private void DrawGameOverScreen()
        {
            GraphicsDevice.Clear(Color.DarkGray);
            spriteBatch.DrawString(gameFont, "Final Score: " + Score, new Vector2(300, 200), Color.Black);
            spriteBatch.DrawString(gameFont, "Press Start or Enter to play again", new Vector2(300, 230), Color.Black);
        }

        private void DrawScoreUpdate()
        {
            GraphicsDevice.Clear(Color.DarkGray);
            spriteBatch.DrawString(gameFont, "Score: " + Score, new Vector2(GraphicsDevice.Viewport.Bounds.Left, GraphicsDevice.Viewport.Bounds.Top), Color.Black);
        }

        private void BeginPause(bool UserInitiated)
        {
            paused = true;
            //pausedForGuide = !UserInitiated;
            //TODO: Pause audio playback
            //TODO: Pause controller vibration
        }

        private void EndPause()
        {
            //TODO: Resume audio
            //TODO: Resume controller vibration
            //pausedForGuide = false;
            paused = false;
        }

    }
}
