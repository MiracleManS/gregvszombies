﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GregVsBroccoli
{
    public class Greg
    {
        public Texture2D PlayerTexture;
        public Texture2D OpenMouthTexture;

        public Vector2 Position;

        public Vector2 Velocity;

        public Rectangle BoundingBox;

        public bool Active;

        public int Score;

        public int Health;

        public int oldTime;

        public int newTime;

        protected int frameWidth, frameHeight, elapsedTime, frameTime, frameCount, currentFrame;
        protected bool animated, looping;
        protected Rectangle frameBox;

        public int Width
        {
            get { return PlayerTexture.Width; }
        }

        public int Height
        {
            get { return PlayerTexture.Height; }
        }

        public void Initialize(Texture2D texture, Texture2D texture_open, Vector2 playerPosition, Vector2 velocity, GraphicsDevice device, int health)  
        {
            PlayerTexture = texture;
            OpenMouthTexture = texture_open;

            Position = playerPosition;
            if (Position.X > device.Viewport.TitleSafeArea.Right)
                Position.X = device.Viewport.TitleSafeArea.Right - texture_open.Width;
            if (Position.X < device.Viewport.TitleSafeArea.Left)
                Position.X = device.Viewport.TitleSafeArea.Left + texture_open.Width;

            Active = true;

            Health = health;
            Velocity = velocity;
            Score = health * 10;
            oldTime = 0;
            newTime = 0;
            frameTime = 500;
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 50, 60);
        }

        public void Update(GameTime gt)
        {
            Position = Position + Velocity;
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 50, 60);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gt)
        {
            if ((int)gt.TotalGameTime.TotalSeconds % 2 == 1)
            {
                spriteBatch.Draw(OpenMouthTexture, Position, null, Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
                //oldTime = (int)gt.TotalGameTime.TotalMilliseconds + frameTime;
            }
            else
            {
                spriteBatch.Draw(PlayerTexture, Position, null, Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
                //oldTime = (int)gt.TotalGameTime.TotalMilliseconds + frameTime;   
            }
            
        }
    }
}
