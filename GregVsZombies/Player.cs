﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GregVsBroccoli
{
    class Player
    {

        public Texture2D PlayerTexture;

        public Vector2 Position;

        public Rectangle BoundingBox;

        public bool Active;

        public int Health;

        public int Width
        {
            get { return PlayerTexture.Width; }
        }

        public int Height
        {
            get { return PlayerTexture.Height; }
        }

        public int Velocity { get; set; }

        public void Initialize(Texture2D texture, Vector2 position)
        {
            PlayerTexture = texture;

            Position = position;

            Active = true;

            Health = 20;

            Velocity = 8;

            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 20, 50);
        }

        public void Update(GraphicsDevice device, int velo)
        {
            if (GamePad.GetState(PlayerIndex.One).DPad.Left == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Left) || Keyboard.GetState().IsKeyDown(Keys.A))
            {
                Position.X = Position.X - Velocity;
                if (Position.X < device.Viewport.Bounds.Left)
                    Position.X = device.Viewport.Bounds.Left;
            }
            if (GamePad.GetState(PlayerIndex.One).DPad.Right == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Right) || Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Position.X = Position.X + Velocity;
                if (Position.X > device.Viewport.Bounds.Right - (PlayerTexture.Width - 30))
                    Position.X = device.Viewport.Bounds.Right - (PlayerTexture.Width - 30);


            }

            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 20, 50);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(PlayerTexture, Position, null, Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
        }
    }
}
