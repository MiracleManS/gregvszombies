﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GregVsBroccoli
{
    class Explosion
    {
        public Texture2D PlayerTexture;

        public Texture2D twoTexture;
        public Texture2D oneTexture;


        public Vector2 Position;

        public Rectangle BoundingBox;

        public bool Active;

        public int Health;

        public int Width
        {
            get { return PlayerTexture.Width; }
        }

        public int Height
        {
            get { return PlayerTexture.Height; }
        }

        public void Initialize(Texture2D texture, Texture2D twoHealth, Texture2D onehealth, Vector2 position)
        {
            PlayerTexture = texture;

            twoTexture = twoHealth;

            oneTexture = onehealth;

            Position = position;

            Active = true;

            Health = 10;

            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, texture.Width, texture.Height);
        }

        public void Update(GraphicsDevice device)
        {

            if (Health < 15 && Health   > 7)
            {
                PlayerTexture = twoTexture;
            }

            if (Health < 7)
            {
                PlayerTexture = oneTexture;
            }

            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, PlayerTexture.Width, PlayerTexture.Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(PlayerTexture, Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
