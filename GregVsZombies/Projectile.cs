﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GregVsBroccoli
{
    class Projectile
    {
        public Texture2D projTexture { get; set; }

        public Vector2 Position { get; set; }

        public Vector2 Velocity { get; set; }

        public Rectangle BoundingBox;

        public GameTime gt;


        public void Initialize(Texture2D texture, Vector2 startPosition)
        {
            projTexture = texture;
            Position = startPosition;
            Random r = new Random();
            Velocity = new Vector2(0, -5);
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 20, 40);
        }

        public void Update(GameTime gameTime)
        {
            Position = Position + Velocity;
            BoundingBox = new Rectangle((int)Position.X, (int)Position.Y, 20, 40);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(projTexture, Position, null, Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
        }
    }
}
